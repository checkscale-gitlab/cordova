FROM seevee/android-node:alpine

LABEL authors="Maik Hummel <m@ikhummel.com>, Chris Vincent <seevee@pm.me>"

ENV CORDOVA_VERSION=8.0.0

WORKDIR "/tmp"

RUN npm i -g --unsafe-perm cordova@$CORDOVA_VERSION
